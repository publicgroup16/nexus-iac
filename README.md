# nexus-iac
Nexus Sonatype Infrastructure As Code using [nexus-casc-plugin](https://github.com/AdaptiveConsulting/nexus-casc-plugin)

This will bootstrap Nexus VM up to generating the configurations like proxy, hosted, etc.<br/>
Docker is still on the works...<br/>

![image](https://github.com/rv-ansible/nexus-iac/assets/35533668/afc87414-7881-4556-80f1-1dc93ceeab3a)

### Prerequisites
ansible 2.16.1+
```
cd nexus-iac
rm -fr ~/.ansible/roles/rv-common || echo .
sleep 1
ansible-galaxy install -r roles/requirements.yml
```
***No need for this on Ansible Tower but needed on Ansible CLI***

### Requirements
- centos8/stream with sudo access
- internet access
- dns server, if none, use ansible inventory

_If firewalled, you need these binaries, 
unfortunately you have to do it, 
please add it and create a pull request,_
```
- epel-release
- nexus-3.62.0-01-unix.tar.gz
- java-1.8.0-openjdk.x86_64
- git
- maven
```

### Instructions

On <home>/.bashrc of ansible, add,
```
export ansible_user=xxx
export ansible_password=xxx
```
Make sure ansible_user have sudo access on the target vm.

Run the ansible in cli,
```
ansible-playbook -i inventory/linux.ini deploy.yml -v --tags deploy_centos8
```

***I did not add sudo access on nexus user, it still works, I tested pull on the docker proxy and push on the docker hosted.***<br/>
***Make sure the Nexus Sonatype version is the same as the nexus-casc-plugin version/tag.***<br/>
***The version or tag is defined on the variable of group_vars/all.***<br/>

### TIPS
***if you only want to test download_package task on iac4***
```
ansible-playbook -i inventory/linux.ini deploy.yml -v --tags iac4 --extra-vars "setup_folder=false setup_startup=false copy_plugin=false iac=false"
```
***to skip allowport if using AWS***
```
ansible-playbook -i inventory/linux.ini deploy.yml -v --extra-vars "allowport=false"
```
