export BASE_URL=http://127.0.0.1:8081
export NEXUS_HOME=/opt/sonatype/nexus
export NEXUS_CASC_CONFIG=/opt/nexus.yml
export NEXUS_UI_PORT=8081
export PLUGIN_VERSION=3.62.0-01
export SONATYPE_DIR=/opt/sonatype
export SONATYPE_WORK=/opt/sonatype/sonatype-work
export HOME=/opt/sonatype/nexus
export PWD=/opt/sonatype
export INSTALL4J_ADD_VM_PARAMS='-Xms2703m -Xmx2703m -XX:MaxDirectMemorySize=2703m -Djava.util.prefs.userRoot=/nexus-data/javaprefs'
export NEXUS_DATA=/nexus-data
export PATH=/home/nexus/.local/bin:/home/nexus/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin 
export OLDPWD=/opt/sonatype