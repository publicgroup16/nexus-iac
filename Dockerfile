ARG version=3.62.0
# hadolint ignore=DL3006
FROM sonatype/nexus3:${version} as BUILD

USER root

COPY nexus-casc-plugin-3.62.0-01-bundle.kar /opt/sonatype/nexus/deploy/nexus-casc-plugin.kar
COPY bashrc /opt/sonatype/nexus/.bashrc


RUN chown -R nexus:nexus /opt/sonatype/nexus && \
    chown -R nexus:nexus /opt/sonatype/sonatype-work && \
    chmod -R 0755 /opt/sonatype/nexus && \
    chmod -R 0755 /opt/sonatype/sonatype-work

COPY --chmod=755 nexus.sh /opt/sonatype/nexus.sh

FROM sonatype/nexus3:${version}

WORKDIR /opt/sonatype/
USER nexus

COPY --from=BUILD /opt/sonatype/ /opt/sonatype/ 
COPY nexus.yml /opt/nexus.yml

ENTRYPOINT ["/opt/sonatype/nexus.sh"]
